当使用多个 epfd (epoll) 实例监听同一个 fd 的时候，EPOLLEXCLUSIVE 参数可以解决惊群效应。
epoll_create 调用多次

当使用多线程或多进程使用 epoll_wait 监控同一个 epfd (epoll) 实例的时候，EPOLLEXCLUSIVE 参数不能解决惊群效应。
相当于是 epoll_create 只调用了一次，多线程或多进程使用 epoll_wait 监控同一个 epfd
使用 EPOLLONESHOT 来解决这个问题，下次使用的时候在使用 EPOLL_MOD 给加回来

有的程序还有问题，跟 telnet 通信不太好
